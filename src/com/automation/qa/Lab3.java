/*3. Найменший елемент масиву замінити цілою частиною середнього арифметичного всіх елементів.
Якщо в масиві є декілька найменших елементів, то замінити останній з них.

 */
package com.automation.qa;

public class Lab3 {

    private static Integer[] myArray = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

    public static void main(String[] args) {
        System.out.println("The min number is " + findMinElement(myArray));
        System.out.println("The arithmetic mean is " + findMeanSumm(myArray));
        replaceMinEl(myArray, findMeanSumm(myArray), findMinElement(myArray));
        for (Integer el : myArray) {
            System.out.println(el);
        }
    }

    private static int findMinElement(Integer[] arr){
        int minElenent = arr[0];
        for (int i = 0; i < arr.length; i++) {
            if (minElenent > arr[i]) minElenent = arr[i];
        }
        return minElenent;
    }

    private static int findMeanSumm(Integer[] arr){
        int summ = 0;
        for (int i = 0; i < arr.length; i++) {
            summ += arr[i];
        }
        int mean = summ / arr.length;
        return mean;
    }

    private static Integer[] replaceMinEl(Integer[] arr, int meanNumber, int minElement){
        for (int i = arr.length - 1 ; i >= 0 ; i--) {
            if (arr[i] == minElement){
                arr[i] = meanNumber;
                break;
            }
        }
        return arr;
    }
}